(setq user-full-name "Michael O'Neil")
(setq user-mail-address "oneil@cims.nyu.edu")

;;
;; path and package setup, load up melpa and marmalade
;;
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize))

(when (<= emacs-major-version 23)
  (load
   (expand-file-name "~/.emacs.d/elpa/package.el"))
  (package-initialize))

(add-to-list 'package-archives 
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives  
	     '("marmalade" . "http://marmalade-repo.org/packages/"))


;;
;; Default packages
;;
(defvar oneil/packages '( auto-complete
                          flycheck
                          auctex
                          gist
                          htmlize
                          magit
                          markdown-mode
                          matlab-mode
                          o-blog
                          org
                          solarized-theme
                          web-mode
                          powerline
                          yaml-mode
			  zenburn-theme)
  "Default packages")

;;
;; Install the packages if not already
;;
(require 'cl)
(defun oneil/packages-installed-p ()
  (loop for pkg in oneil/packages
        when (not (package-installed-p pkg)) do (return nil)
        finally (return t)))

(unless (oneil/packages-installed-p)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (dolist (pkg oneil/packages)
    (when (not (package-installed-p pkg))
      (package-install pkg))))

;;
;; Marking text
;;
(transient-mark-mode t)
(setq x-select-enable-clipboard t)


;;
;; Font and display stuff
;;
(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (set-face-attribute 'default nil
                      :family "Liberation Mono"
                      :height 110
                      :weight 'regular)

  (when (functionp 'set-fontset-font)
    (set-fontset-font "fontset-default"
                      'unicode
                      (font-spec :family "DejaVu Sans Mono"
                                 :width 'normal
                                 :size 12.4
                                 :weight 'normal)))
  )


(setq-default indicate-empty-lines t)
(when (not indicate-empty-lines)
  (toggle-indicate-empty-lines))

(setq ns-use-srgb-colorspace t)
(blink-cursor-mode t)
(setq-default scroll-conservatively 5)

(scroll-bar-mode -1)
(tool-bar-mode -1)


;;
;; Indentation and backup
;;
(setq tab-width 2
      indent-tabs-mode nil)

(setq make-backup-files nil)
(setq auto-save-default nil)


;;
;; Indentation and cleanup commands
;;
(defun small-font ()
  (interactive)
  (set-face-attribute 'default nil
                      :family "Liberation Mono"
                      :height 110
                      :weight 'regular))

(defun big-font ()
  (interactive)
  (set-face-attribute 'default nil
                      :family "Liberation Mono"
                      :height 160
                      :weight 'regular))

(defun untabify-buffer ()
  (interactive)
  (untabify (point-min) (point-max)))

(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer."
  (interactive)
  (indent-buffer)
  (untabify-buffer)
  (delete-trailing-whitespace))

(defun cleanup-region (beg end)
  "Remove tmux artifacts from region."
  (interactive "r")
  (dolist (re '("\\\\│\·*\n" "\W*│\·*"))
    (replace-regexp re "" nil beg end)))

(global-set-key (kbd "C-x M-t") 'cleanup-region)
(global-set-key (kbd "C-c n") 'cleanup-buffer)

(setq-default show-trailing-whitespace nil)


;;
;; miscellaneous settings
;;
(defalias 'yes-or-no-p 'y-or-n-p)
(setq inhibit-startup-message t)
(setq echo-keystrokes 0.1
      use-dialog-box nil
      visible-bell t)
(show-paren-mode t)
(column-number-mode t)
(line-number-mode t)
(setq-default line-spacing 1)
(delete-selection-mode nil)

;;
;; setup flyspell
;;
(setq flyspell-issue-welcome-flag nil)
(if (eq system-type 'darwin)
    (setq-default ispell-program-name "/usr/local/bin/aspell")
  (setq-default ispell-program-name "/usr/bin/aspell"))
(setq-default ispell-list-command "list")


;;
;; Key bindings
;;
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-c C-k") 'compile)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-z") 'goto-line)
(global-set-key (kbd "C-q") 'query-replace)


;;
;; auto-fill settings for line breaks
;;
(add-hook 'f90-mode-hook 'turn-on-auto-fill)

(add-hook 'latex-mode-hook 'turn-on-auto-fill)
(add-hook 'latex-mode-hook 'flyspell-mode)
(add-hook 'tex-mode-hook 'turn-on-auto-fill)

(add-hook 'octave-mode-hook 'turn-on-auto-fill)
(add-hook 'python-mode-hook 'turn-on-auto-fill)

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'text-mode-hook 'flyspell-mode)

(add-hook 'c-mode-hook 'turn-on-auto-fill)
(add-hook 'c++-mode-hook 'turn-on-auto-fill)
(setq fill-column 70)

;;
;; setup auto modes
;;
(add-to-list 'auto-mode-alist '("\\.make\\'" . makefile-mode))
(add-to-list 'auto-mode-alist '("\\.mk\\'" . makefile-mode))
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.yaml$" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.m\\'" . matlab-mode))

;;
;; markdown mode specifics
;;
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.mdown$" . markdown-mode))
(add-hook 'markdown-mode-hook
          (lambda ()
            (visual-line-mode t)
            ;;(writegood-mode t)
            (flyspell-mode t)))
;;(setq markdown-command "pandoc --smart -f markdown -t html")
;;(setq markdown-css-path (expand-file-name "markdown.css" abedra/vendor-dir))



;;
;; fortran specific settings
;;
(add-hook 'f90-mode-hook
      '(lambda () (setq f90-do-indent 2
                        f90-if-indent 2
                        f90-program-indent 2
                        f90-continuation-indent 4
                        ;;f90-comment-region "!!!!"
                        ;;f90-indented-comment-re "!!!!"
                        ;;f90-break-delimiters "[-+\\*/,><=% \t]"
                        ;;f90-break-before-delimiters t
                        f90-beginning-ampersand nil
                        f90-smart-end 'blink
                        f90-auto-keyword-case nil
                        f90-leave-line-no  nil
                        f90-startup-message t
                        indent-tabs-mode nil
                        f90-font-lock-keywords f90-font-lock-keywords-2
                        )
         (f90-add-imenu-menu)        ; extra menu with functions etc.
         ))

;;(setq electric-indent-mode nil)
;;(setq fortran-electric-line-number nil) 
(add-hook 'fortran-mode-hook
      '(lambda () (setq fortran-do-indent 2
                        fortran-if-indent 2
                        fortran-continuation-indent 4
                        fortran-minimum-statement-indent-fixed 0
                        ;;f90-comment-region "!!!!"
                        ;;f90-indented-comment-re "!!!!"
                        ;;f90-break-delimiters "[-+\\*/,><=% \t]"
                        ;;f90-break-before-delimiters t
                        indent-tabs-mode nil
                        fortran-font-lock-keywords-1
                        fortran-font-lock-keywords-2
                        )
         ))


;;
;; org mode specific settings
;;
(add-hook 'org-mode-hook
	  '(lambda () (setq org-log-done 'time
			    )
	     ))


;;  
;; set the theme
;;
(load-theme 'solarized-light t)
;;(load-theme 'zenburn t)

(require 'powerline)
(powerline-default-theme)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" "fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" "282606e51ef2811142af5068bd6694b7cf643b27d63666868bc97d04422318c1" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
