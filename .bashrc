#set term type

# create aliases
alias o='less'
alias make='make -j'
alias ls='ls -F'
alias l='ls -lah'
alias lt='ls -Frt'
alias gp='gnuplot -geometry 650x650 -persist'
alias ens='enscript -o aout.ps -2rG -C '
alias emacs='emacs -nw'

# openmp stuff
export OMP_NUM_THREADS=2
export OMP_STACKSIZE="2048M"
export ARCHFLAGS="-arch x86_64"

# add some stuff to PATH
export PATH=$PATH:.
CASROOT=/usr/local/opt/opencascade

# increase the stacksize
ulimit -s hard

# change ls colors
CLICOLOR="YES"
export CLICOLOR
export TERM=xterm-16color
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# set prompt
#PS1="\u@\h $ "
#PS1="$ "
PS1="[\W $ ] "

# export svn editor and homebrew editor
export SVN_EDITOR='emacs -nw'
export GIT_EDITOR='emacs -nw'
export EDITOR='emacs -nw'

# export homebrew variables
export HOMEBREW_EDITOR='emacs -nw'
export HOMEBREW_USE_GCC=1
export HOMEBREW_VERBOSE=1
export HOMEBREW_GITHUB_API_TOKEN=7a5cb3358c64aa020d894555c86e05b9ed033b1e

export VTK_USE_CARBON=ON
export VTK_USE_COCOA=OFF
export VTK_USE_RPATH=ON
